var resetGrid = function() {
    var out = [];
    for(var i = 0; i < gridLen; i++) {
        var item = [];
        for(var k = 0; k < gridLen; k++) {
            item.push(null);
        }
        out.push(item);
    }
    return out;
}

var createGameBoard = function() {
    var gameBoard = document.querySelector("#game_board");
    for(var i = 0; i < gridLen; i++) {
        var row = createRow(i);
        for(var j = 0; j < gridLen; j++) {
            var col = createCol(i, j);
            row.appendChild(col);
        }
        gameBoard.appendChild(row);
    }
}

var createRow = function(i) {
    var div = document.createElement("div");
    div.setAttribute("data_id", "row_" + i);
    return div;
}

var createCol = function(i, j) {
    var div = document.createElement("div");
    div.setAttribute("data_id", "col_" + j);
    eventGrid[i][j] = function(ev) { markColChecked(i, j); };
    div.addEventListener("click", eventGrid[i][j], false);
    return div;
}

var notify = function(message) {
    var notfication = document.querySelector("#notification");
    notfication.textContent = message;
}

var markColChecked = function(i, j) {
    var row = document.querySelector("div[data_id='row_" + i + "']");
    var col = row.querySelector("div[data_id='col_" + j + "']");
    var hadWon = false;
    grid[i][j] = isFirstActive ? 0 : 1;
    col.appendChild(updateMarkClass());
    col.removeEventListener("click", eventGrid[i][j]);
    eventGrid[i][j] = null;
    moveCounts++;
    if(moveCounts > 4) {
        hadWon = winLogic(i, j);
    }
    if(moveCounts === (gridLen * gridLen) && !hadWon) {
        notify("It is a draw!");
    }
    isFirstActive = !isFirstActive;
}

var updateMarkClass = function() {
    var cls = isFirstActive ? "circle" : "cross";
    var div = document.createElement("div");
    div.classList.add(cls);
    return div;
}

var winLogic = function(i, j) {
    var checkNumber = isFirstActive ? 0 : 1;
    var winMessage = (isFirstActive ? playerOne : playerTwo) + " has won the game";
    var isHorizontal = checkStraights(i, j, checkNumber) && highlightHorizontal(i);
    var isVertical = !isHorizontal && checkVerticals(i, j, checkNumber) && highlightVertical(j);
    var isFirDiag = !isVertical && checkFirstDiagonal(i, j, checkNumber) && highlightFirDiag();
    var isSecDiag = !isFirDiag && checkSecondDiagonal(i, j, checkNumber) && highlightSecDiag();
    var hadWon = isHorizontal || isVertical || isFirDiag || isSecDiag;
    if(hadWon) {
        notify(winMessage);
        wrapUp();
    }
    return hadWon;
}

var highlightHorizontal = function(i) {
    for(var k = 0; k < gridLen; k++) {
        var row = document.querySelector("div[data_id='row_" + i + "']");
        var col = row.querySelector("div[data_id='col_" + k + "']");
        col.classList.add("highlight");
    }
    return true;
}

var highlightVertical = function(j) {
    for(var k = 0; k < gridLen; k++) {
        var row = document.querySelector("div[data_id='row_" + k + "']");
        var col = row.querySelector("div[data_id='col_" + j + "']");
        col.classList.add("highlight");
    }
    return true;
}

var highlightFirDiag = function() {
    for(var k = 0; k < gridLen; k++) {
        var row = document.querySelector("div[data_id='row_" + k + "']");
        var col = row.querySelector("div[data_id='col_" + k + "']");
        col.classList.add("highlight");
    }
    return true;
}

var highlightSecDiag = function() {
    for(var k = 0; k < gridLen; k++) {
        var row = document.querySelector("div[data_id='row_" + k + "']");
        var col = row.querySelector("div[data_id='col_" + (gridLen - (k + 1)) + "']");
        col.classList.add("highlight")
    }
    return true;
}

var checkStraights = function(i, j, matcher) {
    var hadWin = true;
    for(var k = 0; k < gridLen; k++) {
        if(grid[i][k] === matcher) {
            continue;
        } else {
            hadWin = false;
            break;
        }
    }
    return hadWin;
}

var checkVerticals = function(i, j, matcher) {
    var hadWin = true;
    for(var k = 0; k < gridLen; k++) {
        if(grid[k][j] === matcher) {
            continue;
        } else {
            hadWin = false;
            break;
        }
    }
    return hadWin;
}

var checkFirstDiagonal = function(i, j, matcher) {
    var hadWin = i == j;
    if(hadWin) {
        for(var k = 0; k < gridLen; k++) {
            if(grid[k][k] === matcher) {
                continue;
            } else {
                hadWin = false;
                break;
            }
        }
    }
    return hadWin;
}

var checkSecondDiagonal = function(i, j, matcher) {
    var hadWin = (i === 0 && j === (gridLen - 1)) || ((i === gridLen - 1) && j === 0);
    if(hadWin) {
        for(var k = 0; k < gridLen; k++) {
            if(grid[k][gridLen - (k + 1)] === matcher) {
                continue;
            } else {
                hadWin = false;
                break;
            }
        }
    }
    return hadWin;
}

var emptyGameBoard = function() {
    var grid = document.querySelector("#game_board");
    while(grid.firstChild) {
        grid.removeChild(grid.firstChild);
    }
}

var wrapUp = function() {
    for(var i = 0; i < gridLen; i++) {
        for(var j = 0; j < gridLen; j++) {
            if(eventGrid[i][j]) {
                var row = document.querySelector("div[data_id='row_" + i + "']");
                var col = row.querySelector("div[data_id='col_" + j + "']");
                col.removeEventListener("click", eventGrid[i][j]);
            }
        }
    }
}

var reset = function() {
    notify("");
    wrapUp();
    emptyGameBoard();
    init();
}

var gridLen = 3;
var grid = null;
var playerOne = null;
var playerTwo = null;
var moveCounts = 0;
var isFirstActive = true;
var eventGrid = null;
var hadWon = false;

var init = function() {
    grid = resetGrid();
    playerOne = "Player One";
    playerTwo = "Player Two";
    moveCounts = 0;
    isFirstActive = true;
    eventGrid = resetGrid();
    createGameBoard();
}

init(); 