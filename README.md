# TIC TAC TOE

This is a traditional 3X3 grid TIC TAC TOE game, where two players can have a battle of wits.

## How can you start playing?

* Clone the repository by running the command
```
git clone git@bitbucket.org:ssurana2/tic_tac_toe.git tic_tac_toe
```
* Navigate to the `tic_tac_toe` folder
* Open the `index.html` page in any browser.

## Technical Stuff

The game is developed using pure HTML, CSS and JavaScript and no other external plugin is used. Thus can be played in any browser old or new.